#!/bin/sh
# Watch Apk Installer by mikko.timofeev (2019)
# Requirements: adb shell, smart watch

echo
if [ "$(id -u)" != "0" ]; then
   echo 'This script must be run as root'
   echo '[ERR] Exiting...'
   exit 1
fi

if [ -z "$1" ]; then
   echo '[1/5] Select smart watch IP/hostname:'
   read ip
   ip=ip | xargs
   echo "Connecting..."
else
   ip=$1
   echo "[1/4] Connecting to ${ip}..."
fi
if [ -z "$ip" ]; then
   echo '[ERR] Exiting...'
   exit 1
fi
adb connect ${ip}:5555

echo
echo '[2/4] Confirm ADB on smartwatch and press any key'
read -n 1 -s

if [ -z "$2" ]; then
   echo '[3/4] Select APK to install:'
   read -r apk
   apk=apk | xargs
   echo "Installing..." 
else
   apk=$2
   echo "[3/4] Installing APK \"${apk}\"..."
fi

if [ -z "$apk" ]; then
   echo '[ERR] Exiting...'
   exit 1
fi

adb -e install ${apk}

echo '[4/4] Finished'